'use strict';

const fs = require('fs');
const path = require('path');
const toml = require('toml-j0.4');
const locale = require('locale');
const winston = require('winston');

const production = process.env.NODE_ENV === 'production';

let LangApply;

const PreCache = Object.freeze({
    None: 0,
    Default: 1,
    All: 2,
});

let defaultOptions = {
    shouldCache: production,
    defaultLocale: 'en',
    preCache: production ? PreCache.All : PreCache.None,
    directory: path.resolve('./lang'),
    loggingLevel: 'info',
};

/**
 * Applies custom options to default options.
 * @param {object} options the options which will overwrite the default ones.
 * @return {object} the resulting options
*/
defaultOptions.apply = function(options) {
    if (!options) {
        return defaultOptions;
    }

    let joined = {};

    for (const key of Object.keys(this)) {
        if(key === 'directory' && options[key]) {
            options[key] = path.resolve(options[key]);
        }

        if (options[key]) {
            joined[key] = options[key];
        } else {
            joined[key] = defaultOptions[key];
        }
    }

    return joined;
};

/**
 * Initialize langapply with given options.
 * @param {object} options the options to apply. Will use default if some
 * keys are missing or options is undefined.
*/
const initialize = (options) => {
    let instance;

    LangApply = (function() {
        if (instance) {
            throw new AlreadyInitializedError();
        }

        const fixedOptions = defaultOptions.apply(options);

        const logger = winston.createLogger({
            level: fixedOptions.loggingLevel,
            format: winston.format.json(),
            transports: [
                new winston.transports.Console({
                    format: winston.format.simple()
                })
            ]
        });

        let newInstance;

        newInstance = {
            cache: {},
            options: fixedOptions,
            logger: logger
        };

        instance = newInstance;

        return instance;
    })();

    const cache = (file, recursive=false, whitelist=undefined) => {
        // Cache file or recursive if directory
        fs.lstat(file, (err, stats) => {
            if (err) {
                LangApply.logger.error(`Error while getting stat of file ${file}! ${err}`);
                return;
            }

            if (stats.isFile()) {
                if (!file.endsWith('.toml')) {
                    LangApply.logger.debug(`Cannot cache file ${file}, not a valid lang file.`);
                    return;
                }

                // Skip precaching if file is not whitelisted.
                if (whitelist && !whitelist.includes(
                    file.replace('.toml$', '')
                )) {
                    LangApply.logger.debug(`File ${file} is not whitelisted for precaching, skipping.`);
                    return;
                }

                fs.readFile(file, (err, content) => {
                    if (err) {
                        LangApply.logger.error(`Error while reading file ${file}! \
                        ${err}`);
                        return;
                    }

                    const parsed = toml.parse(content.toString());
                    LangApply.cache[file] = parsed;
                    LangApply.logger.debug(`Successfully cached file ${file}, contains keys ${Object.keys(parsed)}`);
                });
            } else if (stats.isDirectory() && recursive) {
                LangApply.logger.debug(`${file} is a dir, caching it...`);
                cacheDir(file, recursive, whitelist);
            }
        });
    };

    const cacheDir = (dirPath, recursive=false, whitelist=undefined) => {
        fs.readdir(dirPath, (err, files) => {
            if (err) {
                LangApply.logger.error(`Error while caching directory '${dirPath}'! ${err}`);
                return;
            }

            LangApply.logger.debug(`Caching directory '${dirPath}'`);

            for (const file of files) {
                const filePath = path.join(dirPath, file);
                cache(filePath, recursive, whitelist);
            }
        });
    };

    if (LangApply.options.preCache) {
        const whitelist = LangApply.options.preCache === PreCache.Default
            ? LangApply.options.defaultLocale
            : undefined;

        cacheDir(
            LangApply.options.directory,
            true,
            whitelist
        );
    }
};

/**
 * Error generated when a lang file (.toml) couldn't be found.
*/
class LangNotFoundError extends Error {
    /**
     * @param {string} url the url for which the file was requested.
     * @param {string} langs a string representing a valid
     * "Accept-Locale" HTTP header.
     */
    constructor(url, langs) {
        super(`Cannot find any of the langs '${langs}' for url '${url}'`);
        Error.captureStackTrace(this, LangNotFoundError);
    }
}

/**
 * An error thrown when function initiaize() is called
 * more than once.
*/
class AlreadyInitializedError extends Error {
    /**
     * @constructor
     */
    constructor() {
        super('LangApply was already initialized and you cannot do it twice!');
        Error.captureStackTrace(this, AlreadyInitializedError);
    }
}

const getDirPath = (url) => {
    // Check correctness
    // Prevent traversal attacks
    url = path.normalize(url || '/')
        .replace(/\.\.+\//, '')
        .replace(/\?.*$/, '');

    const dirPath = path.join(LangApply.options.directory, url);

    return dirPath;
};

/**
 * Escape string for use with RegExp. Taken from hhttps://stackoverflow.com/a/3561711/3797176
 */
RegExp.escape= function(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

/**
 * Get supported locales for given URL from cache.
*/
const getSupportedFromCache = (dirPath) => {
    const regex = new RegExp(`^${RegExp.escape(dirPath)}([a-zA-Z]{2}(?:\\-[a-zA-Z]{2})?).toml$`);

    const supported = Object.keys(LangApply.cache).map((filePath) => {
        const matches = regex.exec(filePath);
        if(matches && matches.length === 2) {
            LangApply.logger.debug(`Found matching locale for path '${dirPath}': ${matches[1]}`)
            return matches[1];
        } else {
            LangApply.logger.debug(`Didn't match ${matches}`);
        }
    }).filter((some) => some);

    return supported.length > 0 ? new locale.Locales(supported) : undefined;
};

/**
 * Get supported langs and calls the callback with (err, supported).
*/
const getSupported = (dirPath, callback) => {
    // First check in the cache. Else use the fs method.
    const fromCache = getSupportedFromCache(dirPath);
    if(fromCache && fromCache.length > 0) {
        LangApply.logger.debug(`Determined supported locales for ${dirPath} from cache. Supported: '${fromCache}'`);
        callback(undefined, fromCache);
        return;
    }

    const regex = new RegExp(`^([a-zA-Z]{2}(?:\\-[a-zA-Z]{2})?).toml$`);

    fs.readdir(dirPath, (err, files) => {
        if(err) {
            callback(err, undefined);
            return;
        }

        // Filter valid toml files, return their langauge.
        const supported = files.map((file) => {
            const matches = regex.exec(file);
            if(matches && matches.length === 2) {
                return matches[1];
            }
        }).filter((some) => some);

        LangApply.logger.debug(`Determined supported locales for ${dirPath} from filesystem. Supported: '${supported}'`);
        callback(undefined, new locale.Locales(supported));
    });
};

const parseFile = (filePath, callback) => {
    fs.readFile(filePath, (err, data) => {
        if (err) {
            if (callback) {
                LangApply.logger.debug(`Couldn't parse file '${filePath}'!`);
                callback(err, undefined);
            }
        } else {
            const parsed = toml.parse(data.toString());
            LangApply.logger.debug(`Parsed file '${filePath}'!`);

            if (LangApply.options.shouldCache) {
                LangApply.cache[filePath] = parsed;
            }

            if (callback) {
                callback(undefined, parsed);
            }
        }
    });
};

/**
 * Reads the lang file for given url and given langs (chooses the best one).
 * Then calls the callback with (err, parsed, lang).
 * @param {string} url the request's url.
 * @param {string} langs valid accepted locales.
 * @param {function} callback the callback to be called once done.
 */
const readLang = (url, langs, callback) => {
    const dirPath = getDirPath(url);

    fs.readdir(dirPath, (err, files) => {
        if (err) {
            LangApply.logger.error(`Cannot read lang directory '${dirPath}'!`);
            return;
        }

        // Choose right langauge
        const matchedLocale = new locale.Locales(
            langs,
            LangApply.options.defaultLocale
        ).best(new locale.Locales(
            files
            .filter((file) => file.endsWith('.toml'))
            .map((file) => file.replace(/\.[^/.]+$/, ''))
        ));

        const filePath = path.join(dirPath, matchedLocale + '.toml');

        const defaultPath = path.join(
            dirPath,
            LangApply.options.defaultLocale + '.toml'
        );


        const cached = LangApply.cache[filePath];
        if (LangApply.options.shouldCache && LangApply.cache[filePath]) {
            callback(undefined, cached, matchedLocale);
            return;
        }

        LangApply.logger.debug(`Reading Lang. Matched Locale: '${matchedLocale}'; File Path: '${filePath}'; Default Path: '${defaultPath}';`);

        // Parse file
        parseFile(filePath, (err, parsed) => {
            if (err) {
                LangApply.logger.debug(`After failing to parse file, failing silently and applying default values.`);
            }

            // Read default too, add missing entries.
            parseFile(defaultPath, (err, defaultParsed) => {
                if (err) {
                    LangApply.logger.error(`Error while reading default values for path '${filePath}'`);
                    callback(err, undefined, undefined);
                    return;
                }

                let summedUp = {};

                LangApply.logger.debug(`Summing up. Default Parsed: '${defaultParsed}'; Parsed: '${parsed}'`);
                for (const key of Object.keys(defaultParsed)) {
                    summedUp[key] = (parsed && parsed[key])
                        ? parsed[key]
                        : defaultParsed[key];
                }

                // Callback with computed context

                callback(undefined, summedUp, matchedLocale);
            });
        });
    });
};

/** Alternative render function to render templates with
 * a custom context, containing a "lang" object, which is a
 * key => value map containing all localized objects in the page.
*/
const render = (req, res, template, context={}, callback) => {
    // Set accepted langs to query parameter or header
    const langs = (req.query.lang ? req.query.lang + ',' : '')
        + (res.locals.lang ? res.locals.lang + ',' : '')
        + req.header('Accept-Language');

    LangApply.logger.debug(`Rendering url '${req.url}' with langs '${langs}'`);

    const afterParseCallback = (err, parsed, matchedLocale) => {
        if (err) {
            return;
        }

        context.lang = parsed;
        context.__lang = matchedLocale.toString();

        res.render(template, context, callback);
    };

    readLang(req.url, langs, afterParseCallback);
};

/**
 * Middleware used to reroute requests from /<lang>/path/ to /path/, so that
 * search engines can correctly geolocalize parts of the website.
 * 
 * For example, /en/about will be internally rerouted to /about, after
 * loading the correct lang file.
 * If the url doesn't have the <lang> prefix the request is going to be redirected to
 * 
 * Only applies to GET requests.
 * 
 * The 'lang' part must be a string consisting of an ISO-639 language
 * abbreviation and optional two-letter ISO-3166 country code.
 * https://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.10
*/
const middleware = (req, res, next) => {
    // Skip if not GET
    if(req.method !== 'GET') {
        next();
    }

    const languageRegex = /^\/([a-zA-Z]{2}(?:\-[a-zA-Z]{2})?)\/.*$/g;

    let matches = languageRegex.exec(req.url);

    // If url is in format /<lang>/path/morepath/...
    if(matches && matches.length === 2) {
        const language = matches[1];
        LangApply.logger.debug(`Requested url ${req.url} is prefixed by a possible 'lang' part ('${language}').`);
        // Remove lang part (/<lang>)
        const delocalizedUrl = req.url.substr(language.length + 1);
        LangApply.logger.debug(`Rerouting request internally from '${req.url}' to '${delocalizedUrl}'`);

        // Reroute url and set local 'lang' in the response.
        req.url = delocalizedUrl;
        res.locals.lang = language;
        LangApply.logger.debug(`Set local 'lang': '${language}'`);
        next();
    } else {
        const langAccept = req.get('Accept-Language');
        LangApply.logger.debug(`Requested url ${req.url} isn't prefixed by a valid 'lang' part, determining correct one.`);

        if(langAccept) {
            const locales = new locale.Locales(langAccept);
            getSupported(getDirPath(req.url), (err, supported) => {
                if(err || !supported) {
                    LangApply.logger.error(`Error while getting supported locales. ${err}`);
                    next();
                    return;
                }

                const best = locales.best(supported);
                LangApply.logger.debug(`Chosen best locale to be '${best}'.`);
                if(!res.headersSent) {
                    res.redirect(path.join('/', best.toString(), req.url));    
                }
            });
        } else {
            const language = LangApply.options.defaultLocale;
            LangApply.logger.debug(`Chosen default locale ('${language}').`);
            // Redirect to localized page.
            res.redirect(path.join('/', language, req.url));
        }
    }
};

module.exports = {
    render: render,
    readLang: readLang,
    LangNotFoundError: LangNotFoundError,
    AlreadyInitializedError: AlreadyInitializedError,
    initialize: initialize,
    PreCache: PreCache,
    middleware: middleware,
};
