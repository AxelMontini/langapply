# LangApply

A library to create lang objects from locale files (and even to render templates
with such values).

## How does it work

It reads `.toml` files from a specified folder and with given options
(preCaching, caching, default locale to use, ...).

Then it can load the right localized properties for a given URL
and `Accept-Language` HTTP Header.

For example, if configured to feed from `./lang` folder, when asked
to load for url `/subpage` and with locales `it, de;q=0.7, fr;q=0.6`
it will try to load the file `./lang/subpage/it.toml` and pass it to
the given callback. Otherwise it will read `./lang/subpage/<X>.toml`
(where `<X>` is the configured default locale) and do the same.
When a locale file is missing a key, the one from the default locale
is used. Use these two files as an example:

- `it.toml`

```toml
    hello = "Ciao"
```

- `en.toml` (the example *Default Locale*)

```toml
    hello = "Hello"
    world = "World"
```

If `it.toml` is needed, the missing key will be loaded from `en.toml`.
The provided callback will be passed the object

```json
{
    hello: 'Ciao',
    world: 'World'
}
```

## Usage

### Initializing

It can be initialized like this.

```javascript
    const langapply = require('langapply');

    /* ... */
    // How production-mode is detected.
    const production = process.env.NODE_ENV === 'production';

    // These are the default options. Provide your own to modify your experience.
    let defaultOptions = {
        shouldCache: production,
        defaultLocale: 'en',
        preCache: production ? PreCache.All : PreCache.None,
        directory: path.resolve('./lang'),
        loggingLevel: 'info',
    };

    langapply.initialize(defaultOptions); // With default options
```

### Rendering templates

It is meant to be used with [Express](https://expressjs.com/) and
it allows to render templates.

You can render them like this.

```javascript
    // inside your request handler
    langapply.render(
        request,
        response,
        template, // Filename of your template
        context, // The context, context.lang = {/*key-value*/} and context.__lang are inserted.
        callback // The callback to be passed to response.render()
    );
```

It adds a `lang` field to your context and calls `response.render()`.
You can access the values by using `{{ lang.yourkey }}` in case you
are using Handlebars templates.

Also you can access the `__lang` property, which contains the langauge
code that is being rendered. For example, you can use this in your
templates to define the language used in your HTML document.

### Using the middleware

Usage:

```javascript
app.use(langapply.middleware);
```

A good way to get things done is by registering `langapply.middleware`.
This middleware processes every `GET` request and:

1. If present, strips out the langauge prefix and calls `next()`, so that
   the request can be processed by other middlewares. This means that, if
   it receives a request which has `/en/page` as url, it reroutes it internally
   to `/page` by modifying it. Also it sets `response.locales.lang` to the langauge
   being rendered. Note that the page is rendered even if the language prefix isn't
   available.
2. If the language prefix is not valid or present but the header `Accept-Language` is,
   then it determines the best language to choose from and redirects the user to the
   correct page, with a language prefix.
3. If no information is available, the user is simply redirected to the page using
   the default language.
