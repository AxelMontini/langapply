'use strict';

// TODO: Write tests xd
const assert = require('assert');
const langapply = require('../index.js');
const path = require('path');

const defaultLocale = 'en';

const optionsCache = {
    shouldCache: true,
    defaultLocale: defaultLocale,
    preCache: langapply.PreCache.Default,
    directory: path.join(__dirname, 'lang'),
    loggingLevel: 'debug',
};

const acceptLocale = 'de,it;q=0.7,en;q=0.2';

/**
 * A dummy next function, that asserts whether the request's url matches
 * the expected one. Calls *done* once completed.
 * @param {Object} req the request which should have *expectedUrl* as *url*.
 * @param {string} expectedUrl the expected URL.
 * @param {function} done the *done* function to be called once done.
*/
const next = (req, expectedUrl, done) => {
    return () => {
        assert.deepStrictEqual(req.url, expectedUrl);
        done();
    };
};

/**
 * Creates a new dummy request to use for testing.
 * @constructor
 * @param {string} url the 'originalURL' of the request.
 * @param {string} queryLang the accept-locale request header.
 * @param {string} queryLang the 'lang' query parameter.
 */
const dummyRequest = function(url, acceptLocale, queryLang, hasHeaders) {
    return {
        /**
         * Return a specific header of this request.
         * @param {string} name the name of the header to return.
         * @return {string} the value of given header.
        */
        header: function(name) {
            if(name.toLowerCase() === 'Accept-Language'.toLowerCase()) {
                return acceptLocale;
            }
        },
        headers: hasHeaders ? {
            'Accept-Language': acceptLocale,
        } : {},
        get: function(name) {
            if(name.toLowerCase() === 'Accept-Language'.toLowerCase()) {
                return acceptLocale;
            }
        },
        originalURL: url,
        url: url,
        query: {
            lang: queryLang
        },
        method: 'GET',
    };
};

const dummyResponse = {
    render: function (template, context, callback) {
        if(callback) {
            callback(undefined, {template: template, context: context});
        }
    },
    locals: {},
};

beforeEach('initialize no cache', function() {
    langapply.initialize(optionsCache);
});

it('reads langs correctly for path \'/\'', function(done) {
    langapply.readLang('', acceptLocale, (err, lang) => {
        assert.ifError(err);

        assert.strictEqual(lang.hello, 'Ciao');
        assert.strictEqual(lang.world, 'Mondo');
        assert.strictEqual(lang.only_english, 'Available Only In English!');

        done();
    });
});

it('reads langs correctly for path \'/\' from a script in \'./src/\'', function(done) {
    __dirname = 'src';

    langapply.readLang('', acceptLocale, (err, lang) => {
        assert.ifError(err);

        assert.strictEqual(lang.hello, 'Ciao');
        assert.strictEqual(lang.world, 'Mondo');
        assert.strictEqual(lang.only_english, 'Available Only In English!');

        done();
    });
});

it('reads langs correctly for path \'/subpath\'', function(done) {
    langapply.readLang('/subpath', acceptLocale, (err, lang) => {
        assert.ifError(err);

        assert.strictEqual(lang.one, 'Eins');
        assert.strictEqual(lang.two, 'Zwei');
        assert.strictEqual(lang.three, 'Three');
        assert.strictEqual(lang.four, 'Vier');
        assert.strictEqual(lang.only_english, 'Available Only In English!');

        done();
    });
});

it('tries to render a template for path \'/\'', function(done) {
    const context = {
        test: 'test-context',
        dummy: 'dummy-thing'
    };

    langapply.render(
        dummyRequest('/', acceptLocale),
        dummyResponse, 'test_template',
        context,
        (err, output) => {
            assert.ifError(err);

            assert.deepEqual(
                output,
                {
                    context: {
                        __lang: 'it',
                        test: 'test-context',
                        dummy: 'dummy-thing',
                        lang: {
                            hello: 'Ciao',
                            world: 'Mondo',
                            only_english: 'Available Only In English!',
                        },
                    },
                    template: 'test_template',
                }
            );

            done();
        }
    );
});

it('reroutes correctly when used as middleware and accessed by REST client with specific language path (root)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/it/');
    let res = dummyResponse;

    langapply.middleware(req, res, next(req, '/', done));
});

it('reroutes correctly when used as middleware and accessed by REST client without language path (root)', function(done) {
    // Simulate a REST client querying a non localized page with no locales.
    // Should never call next, just redirect to the page with default locale.
    let req = dummyRequest('/');
    let res = dummyResponse;
    res.redirect = (newPath) => {
        assert.deepStrictEqual(newPath, '/en/');
        done();
    };

    langapply.middleware(req, res, undefined);
});

it('reroutes correctly when used as middleware and accessed by a browser with specific language path (root)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/fr/', acceptLocale, undefined, true);
    let res = dummyResponse;

    langapply.middleware(req, res, next(req, '/', done));
});

it('reroutes correctly when used as middleware and accessed by a browser without specific language path (root)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/', 'fr', undefined, true);
    let res = dummyResponse;
    res.redirect = (newPath) => {
        assert.deepStrictEqual(newPath, '/fr/');
        done();
    };

    langapply.middleware(req, res, undefined);
});

it('reroutes correctly when used as middleware and accessed by REST client with specific language path (subpath)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/it/subpath/');
    let res = dummyResponse;

    langapply.middleware(req, res, next(req, '/subpath/', done));
});

it('reroutes correctly when used as middleware and accessed by REST client without language path (subpath)', function(done) {
    // Simulate a REST client querying a non localized page with no locales.
    // Should never call next, just redirect to the page with default locale.
    let req = dummyRequest('/subpath/');
    let res = dummyResponse;
    res.redirect = (newPath) => {
        assert.deepStrictEqual(newPath, '/en/subpath/');
        done();
    };

    langapply.middleware(req, res, undefined);
});

it('reroutes correctly when used as middleware and accessed by a browser with specific language path (subpath)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/it/subpath/', acceptLocale, undefined, true);
    let res = dummyResponse;

    langapply.middleware(req, res, next(req, '/subpath/', done));
});

it('reroutes correctly when used as middleware and accessed by a browser without specific language path (subpath)', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/subpath/', 'de', undefined, true);
    let res = dummyResponse;
    res.redirect = (newPath) => {
        assert.deepStrictEqual(newPath, '/de/subpath/');
        done();
    };

    langapply.middleware(req, res, undefined);
});

it('passes the correct __lang magic variable into the context', function(done) {
    let req = dummyRequest('/', acceptLocale, undefined, true);
    let res = dummyResponse;
    res.render = (template, context, callback) => {
        assert.deepStrictEqual(context.__lang, 'it');
        done();
    };

    res.redirect = (newPath) => {
        let req = dummyRequest(newPath, acceptLocale, undefined, true);
        langapply.middleware(req, res, () => {
            langapply.render(req, res, 'dummy-template', {testContext: 'helloThere'});
        });
    };

    langapply.middleware(req, res, undefined);
});

it('doesn\'t redirect if headers are sent already', function(done) {
    // Simulate a REST client querying a localized page with no locales.
    let req = dummyRequest('/it/');
    let res = dummyResponse;
    res.headersSent = true;
    res.redirect = () => {
        assert.fail('doesn\'t redirect', 'redirects', 'Shouldn\'t redirect when headers are sent already.');
    };


    langapply.middleware(req, res, next(req, '/', done));
});